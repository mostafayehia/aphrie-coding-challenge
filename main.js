globalThis.layOutDay = (events) => {
    if (Array.isArray(events)) {
        resetCalendar()
        showEvents(events);
    }
}

function showEvents(events) {
    let eventsSamples = [
        { start: 10, end: 150 },
        { start: 25, end: 40 },
        { start: 30, end: 90 },
        { start: 60, end: 100 },
        { start: 90, end: 110 },
        { start: 100, end: 150 },
        { start: 110, end: 150 },
        { start: 120, end: 150 },

        { start: 180, end: 210 },
        { start: 190, end: 210 },
        { start: 200, end: 240 },
        { start: 230, end: 300 },

        { start: 300, end: 350 },
        { start: 320, end: 400 },

        { start: 410, end: 460 },

        { start: 470, end: 510 },
        { start: 500, end: 550 },
        { start: 540, end: 600 },

        { start: 650, end: 720 }
    ]

    let sortedEvents = sortEvents(events);
    let eventsGroups = groupEvents(sortedEvents);

    const calendar = document.getElementById('calendar_container');

    renderEvents(calendar, eventsGroups);
    


}

function renderEvents(calendar, groups) {

    let totalShift = 0;

    groups.forEach((group, groupIdx) => {
        const eventGroup = document.createElement("div");
        eventGroup.classList.add("event__group");

        let groupLastEvent = lastEvent(groups[groupIdx]);

        if (groupIdx == 0) {
            eventGroup.style.height = `${groupLastEvent.end}px`;
            eventGroup.style.top = `0px`;
        }
        else {
            let prevGroupLastEvent = lastEvent(groups[groupIdx - 1]);
            let currentGroupLastEvent = lastEvent(groups[groupIdx]);
            eventGroup.style.top = `${group[0].start - prevGroupLastEvent.end + totalShift}px`;
            totalShift += group[0].start - prevGroupLastEvent.end;
            eventGroup.style.height = `${(currentGroupLastEvent.start - group[0].start) + currentGroupLastEvent.duration}px`;
        }

        group.forEach((event, eventIdx) => {

            const eventElem = document.createElement("div");
            eventElem.classList.add("event");
            eventElem.style.boxSizing = "border-box";
            
            addTitle(eventElem, event.title);
            addDescription(eventElem, event.description);

            eventElem.style.height = `${event.end - event.start}px`;

            if (groupIdx === 0) {
                eventElem.style.top = `${event.start}px`;
            } else {
                if (eventIdx == 0) {
                    eventElem.style.top = 0;
                } else {
                    eventElem.style.top = `${event.start - group[0].start}px`
                }
            }
            eventGroup.appendChild(eventElem);
        })

        calendar.appendChild(eventGroup);

    })
}

function resetCalendar() {
    const calendar = document.getElementById('calendar_container');
    while (calendar.firstChild) calendar.removeChild(calendar.lastChild);

}

function sortEvents(events) {
    return events.sort((a, b) => a.start - b.start);
}

function groupEvents(sortedEvents) {

    let groups = [];

    for (let ii = 0, marker = 0; ii < sortedEvents.length; ii++) {
        const currentEvent = sortedEvents[ii];
        if (ii == 0) {
            groups.push([currentEvent])
        } else {
            const shouldBelongToGroup = groups[marker].some((e) => currentEvent.start < e.end);
            if (shouldBelongToGroup) {
                groups[marker].push(currentEvent);
            } else {
                groups.push([currentEvent]);
                marker++;
            }
        }
    }

    return groups
}

function lastEvent(group) {
    let max = { start: 0, end: 0, duration: 0 };

    group.forEach(event => {
        const eventDuration = event.end - event.start;
        if (event.end > max.end)
            max = { ...event, duration: eventDuration }
    });

    return max;
}

function addTitle(parent, title) {
    const titleElem = document.createElement("label");
    titleElem.classList.add("event__title")
    titleElem.textContent = title || 'Sample Item';
    parent.appendChild(titleElem);
}

function addDescription(parent, description) {
    const descriptionElem = document.createElement("p");
    descriptionElem.textContent = description || 'Sample Location'
    descriptionElem.classList.add("event__description");
    parent.appendChild(descriptionElem);
}

